.model small
.code
org 100h
start:
	jmp mulai
nama	    db 13,10,'Nama Anda	     : $'
hp	    db 13,10,'No. HP/Telp    : $'
lanjut	    db 13,10,'LANJUT Tekan y untuk lanjut >>>>>>>>>>>>> $'
tampung_nama	db 30,?,30 dup(?)
tampung_hp	db 13,?,13 dup(?)

a db 01
b db 02
c db 03
d db 04
e db 05
f db 06
g db 07
h db 08
i db 09
j dw 15

daftar1 db 13,10,'+-----------------------------------------------------+'
	db 13,10,'|		DAFTAR MENU STREET BOBA		        |'
	db 13,10,'|---+---------------------------------+---------------+'
	db 13,10,'|No | NAMA				| Harga	       |'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|01 | Tokyo Locama			| Rp. 20.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|02 | Nara Miruku Ban			| Rp. 27.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|03 | Shibuya Fresh Milk		| Rp. 26.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|04 | Shizuoka Ichigo			| Rp. 20.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|05 | Okayama Cookies			| Rp. 27.000,00|'
	db 13,10,'+---+---------------------------------+---------------+','$'

daftar2 db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|No | Nama				| Harga	       |'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|06 | Saitama Miruku			| Rp. 24.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|07 | Hokkaido Choco			| Rp. 23.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|08 | Namba Kawaiipon 			| Rp. 29.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|09 | Harajuku Milk Tea			| Rp. 24.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|10 | Otsu Chocoiishi			| Rp. 26.000,00|'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|11 | Sakai Dogaba	  		| Rp. 26.000,00|'
	db 13,10,'+---+---------------------------------+---------------+','$'

pilih_msk   db 13,10,'Silahkan masukkan No/kode menu yang anda pilih: $'
eror	    db 13,10,'KODE YG ANDA MASUKKAN SALAH $'
success	    db 13,10,'Selamat Anda Berhasil $'

	mulai:
	mov ah,09h
	lea dx,nama
	int 21h
	mov ah,0ah
	lea dx,tampung_nama
	int 21h
	push dx

	mov ah,09h
	mov dx,offset daftar1
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01
	int 21h
	cmp al,'y'
	je page2
	jne error_msg
page2:
	mov ah,09h
	mov dx,offset daftar2
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
	jmp proses
	jne error_msg

error_msg:
	mov ah,09h
	mov dx,offset error_msg
	int 21h
	int 20h

proses:
	mov ah,09h
	mov dx,offset pilih_msk
	int 21h
	mov ah,1
	int 21h
	mov bh,al
	mov ah,1
	int 21h
	mov bl,al
	
	cmp bh,'0'
	cmp bl,'1'
	je hasil1

	cmp bh,'0'
	cmp bl,'2'
	je hasil2

	cmp bh,'0'
	cmp bl,'3'
	je hasil3

	cmp bh,'0'
	cmp bl,'4'
	je hasil4

	cmp bh,'0'
	cmp bl,'5'
	je hasil5

	cmp bh,'0'
	cmp bl,'6'
	je hasil6

	cmp bh,'0'
	cmp bl,'7'
	je hasil7
	
	cmp bh,'0'
	cmp bl,'8'
	je hasil8

	cmp bh,'0'
	cmp bl,'9'
	je hasil9

	;cmp bh,'1'
	;cmp bl,'0'
	;je hasil10

	;cmp bh,'1'
	;cmp bl,'1'
	;je hasil11

	jne error_msg
;--------------------------------------------------------

hasil1:
	mov ah,09h
	lea dx,teks1
	int 21h
	int 20h

hasil2:
	mov ah,09h
	lea dx,teks2
	int 21h
	int 20h

hasil3:
	mov ah,09h
	lea dx,teks3
	int 21h
	int 20h

hasil4:	
	mov ah,09h
	lea dx,teks4
	int 21h
	int 20h

hasil5:
	mov ah,09h
	lea dx,teks5
	int 21h
	int 20h

hasil6:
	mov ah,09h
	lea dx,teks6
	int 21h
	int 20h

hasil7:
	mov ah,09h
	lea dx,teks7
	int 21h
	int 20h

hasil8:
	mov ah,09h
	lea dx,teks8
	int 21h
	int 20h

hasil9:
	mov ah,09h
	lea dx,teks9
	int 21h
	int 20h

hasil10:
	mov ah,09h
	lea dx,teks10
	int 21h
	int 20h	

hasil11:
	mov ah,09h
	lea dx,teks11
	int 21h
	int 20h	


;--------------------------------------------------------

teks1	db 13,10,'Menu yang anda pilih : Tokyo Locama'
	db 13,10,'Total harga yang harus di bayar adalah RP. 20.000,00'
        db 13,10,'modal untuk menu Tokyo Locama adalah Rp. 15.500,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 4.500,00'
	db 13,10,'Terima Kasih $'

teks2	db 13,10,'Menu yang anda pilih : Nara Miruku Ban'
	db 13,10,'Total harga yang harus di bayar adalah RP. 27.000,00'
        db 13,10,'modal untuk menu Nara miruku ban adalah Rp. 19.500,00'
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 7.500,00'
	db 13,10,'Terima Kasih $' 

teks3	db 13,10,'Menu yang anda pilih : Shibuya Fresh Milk'
	db 13,10,'Total harga yang harus di bayar adalah RP. 26.000,00'
        db 13,10,'modal untuk menu Shibuya Fresh Milk adalah Rp. 17.000,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 9.000,00'
	db 13,10,'Terima Kasih $' 

teks4	db 13,10,'Menu yang anda pilih : Shizuoka Ichigo'
	db 13,10,'Total harga yang harus di bayar adalah RP. 20.000,00'
        db 13,10,'modal untuk menu Shizouka Ichigo adalah Rp. 14.000,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 6.000,00'

	db 13,10,'Terima Kasih $'

teks5	db 13,10,'Menu yang anda pilih : Okayama Cookies'
	db 13,10,'Total harga yang harus di bayar adalah RP. 27.000,00'
        db 13,10,'modal untuk menu Okayama cookies adalah Rp. 20.000,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 7.000,00'
	db 13,10,'Terima Kasih $'

teks6	db 13,10,'Menu yang anda pilih : Saitama Miruku'
	db 13,10,'Total harga yang harus di bayar adalah RP. 24.000,00'
        db 13,10,'modal untuk menu Saitama Maariku adalah Rp. 15.000,00'
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 9.000,00'
	db 13,10,'Terima Kasih $'

teks7	db 13,10,'Menu yang anda pilih : Hokkaido Choco'
	db 13,10,'Total harga yang harus di bayar adalah RP. 23.000,00'
        db 13,10,'modal untuk menu Hokaido choco adalah Rp. 17.000,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 6.000,00'
	db 13,10,'Terima Kasih $'

teks8	db 13,10,'Menu yang anda pilih : Namba Kawaiipon'
	db 13,10,'Total harga yang harus di bayar adalah RP. 29.000,00'
        db 13,10,'modal untuk menu Namba kawaiipon adalah Rp. 17.000,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 9.000,00'
	db 13,10,'Terima Kasih $'

teks9	db 13,10,'Menu yang anda pilih : Harajuku Milk Tea'
	db 13,10,'Total harga yang harus di bayar adalah RP. 24.000,00'
        db 13,10,'modal untuk menu Harajuku Milk tea adalah Rp. 17.000,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 7.000,00'
	db 13,10,'Terima Kasih $'

teks10	db 13,10,'Menu yang anda pilih : Otsu Chocoiishi'
	db 13,10,'Total harga yang harus di bayar adalah RP. 26.000,00'
        db 13,10,'modal untuk menu otsu chocoiishi adalah Rp. 18.000,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 8.000,00'
	db 13,10,'Terima Kasih $'

teks11	db 13,10,'Menu yang anda pilih : Sakai Dogaba'
	db 13,10,'Total harga yang harus di bayar adalah RP. 26.000,00'
        db 13,10,'modal untuk menu Sakai Dogaba adalah Rp. 17.000,00' 
        db 13,10,'Maka keuntungan dari penjualan adalah Rp. 9.000,00'
	db 13,10,'Terima Kasih $'

end start